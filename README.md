## Prometheus-Exporters
### Blackbox-Exporter
>為官方推出的 Prometheus Exporter <br>
>主要可透過第三地探測監控目標的狀態 <br>
>主要透過 HTTP, HTTPS, DNS, TCP and ICMP 進行探測
---
### Node-Exporter
>官方的 Prometheus Exporter [node-export](https://github.com/prometheus/node_exporter) 主要探測系統狀態<br>
>啟動已置入docker-compose.yml <br>
>版號 prom/node-exporter:v1.2.2 expose port :9110 <br>
- Confirm <br>
`curl http://localhost:9110/metrics`
---
### Process-Exporter
>3rd' Prometheus exporter [process-exporter](https://github.com/ncabatoff/process-exporter) <br>
>主要工作原理為截取 系統/proc 下PID裡的數值<br>
>版號 process-exporter:v0.7.5 default expose port :9256
- Path config/process-exporter.yaml
```
process_names:
  - name: "{{.Matches}}"
    cmdline:
    - 'node'
    - 'monit.js'
```
- 可用 `ps -ef` 作執行程序確認
>cmdline: <br>
> \-  'node' <--- 匹配執行程序字串 <br>
> \-  'monit.js' <--- 匹配同一程序第二個字串
- Confirm <br>
`curl http://localhost:9256/metrics`

---
### Installation
- requirement <br>
[docker install](https://docs.docker.com/engine/install/)
- file description (此目錄結構是為了因應後續擴展其它 Exporter)<br>
```
├── blackbox-exporter.tar
├── exporter
│   ├── blackbox_exporter
│   │   └── blackbox.yml
│   ├── docker-compose.yml
│   └── process_exporter
│       └── process-exporter.yaml
├── load_docker_image.sh
├── node-exporter.tar
└── process-exporter.tar
```
1. `blackbox-export.tar` <br>
blackbox-exporter docker image
2. `blackbox.yml` <br>
blackbox-exporter modules [config official refer](https://github.com/prometheus/blackbox_exporter/tree/master/config/testdata)
3. `process-exporter.yaml` <br>
process-exporter config define process match [3rd' exporter](https://github.com/ncabatoff/process-exporter)
4. `docker-compose.yml` <br>
docker compose yml file
5. `load_docker_image.sh` <br>
	- load blackbox docker image
	- load node-exporter docker image
    - load process-exporter docker images
	- docker compose up
	- remove tar files
6. `node-exporter.tar` <br>
node-exporter docker image ver:v1.5.0 (file是v1.2.2)

### Running

```
$ cd exporter
$ docker-compose up -d
```
### Stop & Down
```
$ cd exporter
$ docker-compose stop
or
$ docker-compose down
```


