#!/bin/bash -e
docker load -i blackbox-exporter.tar
docker load -i node-exporter.tar
docker load -i process-exporter.tar
docker load -i caddy.tar
./init_caddyfile.sh
cd exporter/
docker-compose up -d
cd ../
[ -f blackbox-exporter.tar ] && rm -f blackbox-exporter.tar
[ -f node-exporter.tar ] && rm -f node-exporter.tar
[ -f process-exporter.tar ] && rm -f process-exporter.tar
[ -f caddy.tar ] && rm -f caddy.tar

