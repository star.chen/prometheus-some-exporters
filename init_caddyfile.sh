#!/bin/bash -e
if [[ $(ip addr|grep eth0) ]];
then
IP=$(ifconfig eth0 |grep 'inet '|awk 'BEGIN{FS=" "} {print $2}')
mkdir exporter/caddy
touch exporter/caddy/Caddyfile
echo ":9110 {
    reverse_proxy ${IP}:9100
}" > exporter/caddy/Caddyfile
else
echo " Please check your network name is eth0? "
fi
